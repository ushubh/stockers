package com.shubh.stocker.data;

import com.shubh.stocker.core.data.Local;
import com.shubh.stocker.core.data.Remote;
import com.shubh.stocker.data.local.StockLocalDataSource;
import com.shubh.stocker.data.remote.FakeStockRemoteDataSource;
import com.shubh.stocker.data.remote.StockRemoteDataSource;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class StockRepositoryModule {

    @Singleton
    @Binds
    @Local
    public abstract StockDataSource provideTasksLocalDataSource(StockLocalDataSource stockLocalDataSource);

    @Singleton
    @Provides
    @Remote
    public abstract StockDataSource provideTasksRemoteDataSource(FakeStockRemoteDataSource stockRemoteDataSource);


//    @Singleton
//    @Provides
//    AppExecutors provideAppExecutors() {
//        return new AppExecutors(new DiskIOThreadExecutor(),
//                Executors.newFixedThreadPool(THREAD_COUNT),
//                new AppExecutors.MainThreadExecutor());
//    }
}