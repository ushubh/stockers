package com.shubh.stocker.data.remote;

import com.shubh.stocker.beans.Company;
import com.shubh.stocker.beans.Stock;
import com.shubh.stocker.core.data.Remote;
import com.shubh.stocker.data.StockDataSource;
import com.shubh.stocker.services.StockService;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Remote
@Singleton
public class FakeStockRemoteDataSource implements StockDataSource {

    private StockService mStockService;

    @Inject
    public FakeStockRemoteDataSource(StockService stockService) {
        this.mStockService = stockService;
    }

    @Override
    public Flowable<Stock> loadStock(boolean forceRemote, String stockSymbol) {
        return mStockService.getStockDetails(stockSymbol);
    }

    @Override
    public Flowable<Company> getCompany(String stockSymbol) {
        return mStockService.getCompanyDetails(stockSymbol);
    }

    @Override
    public void addStock(String stockSymbol, Stock question) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void clearData() {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void addCompany(Company company) {

    }

    @Override
    public Flowable<String> getSelectedStockSymbol() {
        throw new UnsupportedOperationException("Unsupported operation");
    }

}