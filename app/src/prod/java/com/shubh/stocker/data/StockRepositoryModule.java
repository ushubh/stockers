package com.shubh.stocker.data;

import com.shubh.stocker.core.data.Local;
import com.shubh.stocker.core.data.Remote;
import com.shubh.stocker.data.local.StockLocalDataSource;
import com.shubh.stocker.data.remote.StockRemoteDataSource;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class StockRepositoryModule {

    @Singleton
    @Binds
    @Local
    public abstract StockDataSource provideTasksLocalDataSource(StockLocalDataSource stockLocalDataSource);

    @Singleton
    @Binds
    @Remote
    public abstract StockDataSource provideTasksRemoteDataSource(StockRemoteDataSource stockRemoteDataSource);

}