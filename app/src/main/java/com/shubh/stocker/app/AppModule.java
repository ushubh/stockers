package com.shubh.stocker.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.shubh.stocker.core.api.RetrofitHelper;
import com.shubh.stocker.data.StockRepositoryModule;
import com.shubh.stocker.services.StockService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Module(includes = {
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        StockRepositoryModule.class,
        ActivityModule.class}
)
class AppModule {

    private static final String PREF_NAME = "stock";

    @Provides
    @Singleton
    Context provideContext(App app) {
        return app;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return RetrofitHelper.getGson();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return RetrofitHelper.getHttpClient();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return RetrofitHelper.getRetrofit(gson, okHttpClient);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(App app) {
        return app.getSharedPreferences(PREF_NAME, 0);
    }

    @Provides
    @Singleton
    StockService provideStockService(Retrofit retrofit) {
        return retrofit.create(StockService.class);
    }

}
