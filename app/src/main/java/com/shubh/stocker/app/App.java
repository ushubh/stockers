package com.shubh.stocker.app;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import com.shubh.stocker.scheduler.SchedulerUtil;
import com.shubh.stocker.scheduler.StockSchedulerService;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import timber.log.Timber;

public class App extends Application implements HasActivityInjector, HasServiceInjector {

    private static final int STOCK_SCHEDULER_ID = 10;

    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    @Inject
    DispatchingAndroidInjector<Service> serviceInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent.builder().application(this).build().inject(this);
        Timber.plant(new Timber.DebugTree());
        Timber.tag(this.getClass().getName());

        if (!SchedulerUtil.isJobScheduled(this, STOCK_SCHEDULER_ID)) {
            int result = SchedulerUtil.schedule(this, STOCK_SCHEDULER_ID, StockSchedulerService.class);
            if (result == 1) {
                Timber.d("Job Scheduled Successfully");
            }
        }
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceInjector;
    }
}
