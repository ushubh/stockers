package com.shubh.stocker.app;


import com.shubh.stocker.core.scope.PerActivity;
import com.shubh.stocker.scheduler.StockSchedulerService;
import com.shubh.stocker.settings.SettingsActivity;
import com.shubh.stocker.settings.SettingsActivityModule;
import com.shubh.stocker.stock.StockActivity;
import com.shubh.stocker.stock.StockActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = {StockActivityModule.class})
    @PerActivity
    protected abstract StockActivity bindStatisticsActivity();

    @ContributesAndroidInjector(modules = {SettingsActivityModule.class})
    @PerActivity
    protected abstract SettingsActivity bindSettingsActivity();

    @ContributesAndroidInjector()
    protected abstract StockSchedulerService bindJobService();
}

