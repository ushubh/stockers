package com.shubh.stocker.services;

import com.shubh.stocker.beans.Company;
import com.shubh.stocker.beans.Statistic;
import com.shubh.stocker.beans.Stock;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface StockService {

    @GET("/1.0/stock/{symbol}/company")
    Flowable<Company> getCompanyDetails(@Path("symbol") String symbol);

    @GET("/1.0/stock/{symbol}/quote?displayPercent=true")
    Flowable<Stock> getStockDetails(@Path("symbol") String symbol);

    @GET("/1.0/stock/{symbol}/chart/{range}")
    Flowable<List<Statistic>> getStatistics(@Path("symbol") String symbol, @Path("range") String range);
}
