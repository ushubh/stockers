package com.shubh.stocker.stock;

import com.shubh.stocker.core.presenter.BasePresenter;
import com.shubh.stocker.core.view.BaseContentView;
import com.shubh.stocker.response.StockResponse;

public interface StockContract {

    interface View extends BaseContentView {

        void onStockReceived(StockResponse stockResponse);
    }

    interface Presenter extends BasePresenter<View> {

        void loadStockStatistic(boolean forceLoad);
    }
}
