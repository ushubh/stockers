package com.shubh.stocker.stock;

import com.shubh.stocker.R;
import com.shubh.stocker.core.activity.BaseActivity;

public class StockActivity extends BaseActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_main;
    }
}
