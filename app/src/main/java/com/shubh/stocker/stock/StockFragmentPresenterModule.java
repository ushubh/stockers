package com.shubh.stocker.stock;

import com.shubh.stocker.core.scope.PerFragment;

import dagger.Binds;
import dagger.Module;

@Module(includes = StockFragmentModule.class)
public abstract class StockFragmentPresenterModule {

    @Binds
    @PerFragment
    public abstract StockContract.Presenter StockPresenter(StockFragmentPresenter stockFragmentPresenter);
}
