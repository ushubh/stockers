package com.shubh.stocker.stock;


import com.shubh.stocker.core.presenter.AbsPresenter;
import com.shubh.stocker.core.scope.PerFragment;
import com.shubh.stocker.data.StockRepository;
import com.shubh.stocker.response.StockResponse;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@PerFragment
public class StockFragmentPresenter extends AbsPresenter<StockContract.View> implements StockContract.Presenter {

    private StockRepository mStockRepository;

    private Disposable mDisposable;

    @Inject
    StockFragmentPresenter(StockRepository stockRepository, StockContract.View view) {
        super(view);
        this.mStockRepository = stockRepository;
    }

    @Override
    public void loadStockStatistic(boolean forceLoad) {
        mDisposable = mStockRepository.getSelectedStockSymbol()
                .doOnSubscribe(subscription -> {
                    getView().showProgress();
                    getView().hideContent();
                })
                .flatMap(stockSymbol ->
                        Flowable.zip(
                                mStockRepository.getCompany(stockSymbol).subscribeOn(Schedulers.io()),
                                mStockRepository.loadStock(forceLoad, stockSymbol).subscribeOn(Schedulers.io()),
                                mStockRepository.getStatistic(false, "1d", stockSymbol).subscribeOn(Schedulers.io()),
                                (company, stock, statistics) -> new StockResponse(stock, company, statistics)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stockResponse -> {
                    getView().showContent();
                    getView().hideRetry();
                    getView().onStockReceived(stockResponse);
                }, throwable -> {
                    Timber.e(throwable);
                    getView().showRetry();
                    getView().showError();
                }, () -> {
                    getView().hideProgress();
                    Timber.d("Load stock statistic complete");
                });
    }

    @Override
    public void onViewDetached() {
        super.onViewDetached();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }
}
