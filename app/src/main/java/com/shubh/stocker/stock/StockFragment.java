package com.shubh.stocker.stock;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.shubh.stocker.R;
import com.shubh.stocker.beans.Statistic;
import com.shubh.stocker.beans.Stock;
import com.shubh.stocker.core.fragments.ContentFragment;
import com.shubh.stocker.response.StockResponse;
import com.shubh.stocker.settings.SettingsActivity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class StockFragment extends ContentFragment implements StockContract.View {

    private static final int REQUEST_CODE_SETTING_ACTIVITY = 101;

    @BindView(R.id.companyName)
    TextView mCompanyName;

    @BindView(R.id.stockSymbol)
    TextView mStockSymbol;

    @BindView(R.id.currentPrice)
    TextView mCurrentPrice;

    @BindView(R.id.openValue)
    TextView mOpenValue;

    @BindView(R.id.volume)
    TextView mVolume;

    @BindView(R.id.closeValue)
    TextView mCloseValue;

    @BindView(R.id.diffPercentage)
    TextView mDiffPercentage;

    @BindView(R.id.lineChart)
    LineChart mLineChart;

    @Inject
    StockContract.Presenter mStockPresenter;

    @Override
    protected int getContentLayoutResId() {
        return R.layout.fragment_stock_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLineChart.setDrawGridBackground(false);
        mLineChart.getDescription().setEnabled(false);
        mLineChart.setTouchEnabled(true);
        mLineChart.setDragEnabled(true);
        mLineChart.setScaleEnabled(true);
        mLineChart.setPinchZoom(true);

        YAxis leftAxis = mLineChart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.disableAxisLineDashedLine();
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(true);

        mLineChart.getAxisRight().setEnabled(false);

        mLineChart.getXAxis().setDrawGridLines(false);
        mLineChart.getXAxis().setDrawLabels(false);
        mLineChart.getXAxis().setDrawAxisLine(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_stock_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivityForResult(intent, REQUEST_CODE_SETTING_ACTIVITY);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SETTING_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                mStockPresenter.loadStockStatistic(true);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mStockPresenter.onViewDetached();
    }

    @Override
    protected void loadData() {
        mStockPresenter.loadStockStatistic(false);
    }

    @Override
    protected void onRetry() {
        mStockPresenter.loadStockStatistic(false);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onStockReceived(StockResponse stockResponse) {
        mCompanyName.setText(stockResponse.getCompany().getCompanyName());
        mStockSymbol.setText(stockResponse.getCompany().getSymbol());

        Stock stock = stockResponse.getStock();
        mCurrentPrice.setText(String.valueOf(stock.getLatestPrice()));

        mOpenValue.setText(String.valueOf(stock.getOpen()));

        mVolume.setText(String.valueOf(stock.getLatestVolume()));
        mDiffPercentage.setText(getString(R.string.percentage_diff, round(stock.getChangePercent())));

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour >= 9 && hour < 17) {
            // stock is open
            mCloseValue.setText("- - -");
        } else {
            mCloseValue.setText(String.valueOf(round(stock.getClose())));
        }

        int result = stock.getLatestPrice().compareTo(stock.getPreviousClose());
        int drawableId = R.drawable.ic_arrow_drop_up;
        if (result < 0) {
            // Going down
            drawableId = R.drawable.ic_arrow_drop_down;
        }

        mDiffPercentage.setCompoundDrawablesWithIntrinsicBounds(drawableId, 0, 0, 0);

        // Add chart data
        mLineChart.animateX(2500);
        List<Statistic> statistics = stockResponse.getStatistics();
        setData(statistics);
    }

    private void setData(List<Statistic> statistics) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int i = 0; i < statistics.size() - 1; i++) {
            Statistic statistic = statistics.get(i);
            values.add(new Entry(i, statistic.getMarketAverage().floatValue()));
        }

        LineDataSet lineDataSet;
        if (mLineChart.getData() != null && mLineChart.getData().getDataSetCount() > 0) {
            lineDataSet = (LineDataSet) mLineChart.getData().getDataSetByIndex(0);
            lineDataSet.setValues(values);
            mLineChart.getData().notifyDataChanged();
            mLineChart.notifyDataSetChanged();
            return;
        }

        // create a dataset and give it a type
        lineDataSet = new LineDataSet(values, "");
        lineDataSet.setDrawIcons(false);
        lineDataSet.setDrawHighlightIndicators(false);
        lineDataSet.setColor(ActivityCompat.getColor(getContext(), R.color.primaryColor));
        lineDataSet.setLineWidth(1f);
        lineDataSet.setValueTextSize(9f);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFormLineWidth(1f);
        lineDataSet.setFormSize(15.f);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircles(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);
        LineData data = new LineData(dataSets);
        mLineChart.setData(data);
        mLineChart.invalidate();
    }

    public static double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
