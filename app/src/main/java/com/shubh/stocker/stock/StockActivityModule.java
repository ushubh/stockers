package com.shubh.stocker.stock;

import com.shubh.stocker.core.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class StockActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {StockFragmentPresenterModule.class})
    abstract public StockFragment stockFragment();
}
