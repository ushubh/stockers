package com.shubh.stocker.stock;

import com.shubh.stocker.core.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class StockFragmentModule {

    @Provides
    @PerFragment
    StockContract.View provideStockView(StockFragment stockFragment) {
        return stockFragment;
    }
}
