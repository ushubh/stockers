package com.shubh.stocker.data;

import com.shubh.stocker.beans.Company;
import com.shubh.stocker.beans.Statistic;
import com.shubh.stocker.beans.Stock;

import java.util.List;

import io.reactivex.Flowable;

public interface StockDataSource {

    Flowable<Stock> loadStock(boolean forceRemote, String stockSymbol);

    Flowable<String> getSelectedStockSymbol();

    Flowable<Company> getCompany(String stockSymbol);

    Flowable<List<Statistic>> getStatistic(boolean forceLoad, String range, String symbol);

    void addStock(String stockSymbol, Stock question);

    void addCompany(Company company);

    void addStatistic(String stockSymbol, List<Statistic> statistic);

    void updateStockSymbol(String symbol);

    void clearData();

}