package com.shubh.stocker.data.remote;

import com.shubh.stocker.beans.Company;
import com.shubh.stocker.beans.Statistic;
import com.shubh.stocker.beans.Stock;
import com.shubh.stocker.core.data.Remote;
import com.shubh.stocker.data.StockDataSource;
import com.shubh.stocker.services.StockService;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Remote
@Singleton
public class StockRemoteDataSource implements StockDataSource {

    private StockService mStockService;

    @Inject
    public StockRemoteDataSource(StockService stockService) {
        this.mStockService = stockService;
    }

    @Override
    public Flowable<Stock> loadStock(boolean forceRemote, String stockSymbol) {
        return mStockService.getStockDetails(stockSymbol);
    }

    @Override
    public Flowable<Company> getCompany(String stockSymbol) {
        return mStockService.getCompanyDetails(stockSymbol);
    }

    @Override
    public Flowable<List<Statistic>> getStatistic(boolean forceLoad, String range, String symbol) {
        return mStockService.getStatistics(symbol, range);
    }

    @Override
    public void addStock(String stockSymbol, Stock question) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void clearData() {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void addCompany(Company company) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void addStatistic(String stockSymbol, List<Statistic> statistic) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void updateStockSymbol(String symbol) {
        //Currently, we do not need this.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Flowable<String> getSelectedStockSymbol() {
        throw new UnsupportedOperationException("Unsupported operation");
    }

}