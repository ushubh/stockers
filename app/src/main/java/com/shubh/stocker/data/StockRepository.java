package com.shubh.stocker.data;

import android.text.TextUtils;

import com.shubh.stocker.beans.Company;
import com.shubh.stocker.beans.Statistic;
import com.shubh.stocker.beans.Stock;
import com.shubh.stocker.core.data.Local;
import com.shubh.stocker.core.data.Remote;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class StockRepository implements StockDataSource {

    private StockDataSource remoteDataSource;

    private StockDataSource localDataSource;

    @Inject
    StockRepository(@Local StockDataSource localDataSource, @Remote StockDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public Flowable<Stock> loadStock(boolean forceRemote, String stockSymbol) {

        if (forceRemote) {
            return refreshData(stockSymbol);
        }
        return localDataSource.loadStock(false, stockSymbol)
                .flatMap(stock -> {
                    if (!TextUtils.isEmpty(stock.getSymbol())) {
                        return Flowable.just(stock);
                    } else {
                        return refreshData(stockSymbol);
                    }
                });
    }

    private Flowable<Stock> refreshData(String stockSymbol) {
        return remoteDataSource.loadStock(true, stockSymbol)
                .doOnNext(stock -> {
                    localDataSource.clearData();
                    localDataSource.addStock(stockSymbol, stock);
                }).flatMap(Flowable::just);
    }

    @Override
    public void clearData() {
        localDataSource.clearData();
    }

    @Override
    public Flowable<String> getSelectedStockSymbol() {
        return localDataSource.getSelectedStockSymbol();
    }

    @Override
    public Flowable<Company> getCompany(String stockSymbol) {
        return localDataSource.getCompany(stockSymbol)
                .flatMap(company -> {
                    if (!TextUtils.isEmpty(company.getSymbol())) {
                        return Flowable.just(company);
                    } else {
                        return fetchCompanyDetails(stockSymbol);
                    }
                });
    }

    public Flowable<Company> fetchCompanyDetails(String stockSymbol) {
        return remoteDataSource.getCompany(stockSymbol)
                .doOnNext(company -> {
                    localDataSource.addCompany(company);
                    localDataSource.updateStockSymbol(company.getSymbol());
                })
                .flatMap(Flowable::just);
    }

    @Override
    public Flowable<List<Statistic>> getStatistic(boolean forceLoad, String range, String symbol) {
        if (forceLoad) {
            return fetchStatistics(symbol, range);
        }
        return localDataSource.getStatistic(false, range, symbol)
                .filter(statistics -> !statistics.isEmpty())
                .switchIfEmpty(fetchStatistics(symbol, range));
    }

    private Flowable<List<Statistic>> fetchStatistics(String symbol, String range) {
        return remoteDataSource.getStatistic(false, range, symbol)
                .doOnNext(statistics -> localDataSource.addStatistic(symbol, statistics));
    }


    @Override
    public void addStock(String stockSymbol, Stock question) {
        //Currently, we do not need this.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void addCompany(Company company) {
        //Currently, we do not need this.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void addStatistic(String stockSymbol, List<Statistic> statistic) {
        //Currently, we do not need this.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void updateStockSymbol(String symbol) {
        //Currently, we do not need this.
        throw new UnsupportedOperationException("Unsupported operation");
    }


}