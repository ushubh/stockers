package com.shubh.stocker.data.local;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shubh.stocker.beans.Company;
import com.shubh.stocker.beans.Statistic;
import com.shubh.stocker.beans.Stock;
import com.shubh.stocker.core.data.Local;
import com.shubh.stocker.data.StockDataSource;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

@Local
@Singleton
public class StockLocalDataSource implements StockDataSource {

    private static final String KEY_SELECTED_STOCK_SYMBOL = "com.shubh.stocker.SELECTED_STOCK_SYMBOL";
    private static final String KEY_COMPANY_PREFIX = "company";
    private static final String KEY_STOCK_PREFIX = "stock";
    private static final String KEY_STATISTIC_PREFIX = "statistic";

    private static final String DEFAULT_STOCK_SYMBOL = "GOOG";

    private SharedPreferences mSharedPreferences;
    private Gson mGson;

    @Inject
    StockLocalDataSource(SharedPreferences sharedPreferences, Gson gson) {
        this.mSharedPreferences = sharedPreferences;
        this.mGson = gson;
    }

    @Override
    public Flowable<Stock> loadStock(boolean forceRemote, String stockSymbol) {
        return Flowable.create(emitter -> {
            Stock stock = new Stock();
            String stockData = mSharedPreferences.getString(KEY_STOCK_PREFIX + stockSymbol, null);

            if (!TextUtils.isEmpty(stockData)) {
                Gson gson = new Gson();
                stock = gson.fromJson(stockData, Stock.class);
            }
            emitter.onNext(stock);
            emitter.onComplete();

        }, BackpressureStrategy.LATEST);

    }

    @Override
    public void addStock(String stockSymbol, Stock question) {
        mSharedPreferences.edit().putString(KEY_STOCK_PREFIX + stockSymbol, mGson.toJson(question)).apply();
    }

    @Override
    public Flowable<String> getSelectedStockSymbol() {
        return Flowable.create(emitter -> {
            String stockSymbol = mSharedPreferences.getString(KEY_SELECTED_STOCK_SYMBOL, null);

            if (TextUtils.isEmpty(stockSymbol)) {
                stockSymbol = DEFAULT_STOCK_SYMBOL;
                mSharedPreferences.edit().putString(KEY_SELECTED_STOCK_SYMBOL, stockSymbol);
            }
            emitter.onNext(stockSymbol);
            emitter.onComplete();

        }, BackpressureStrategy.LATEST);
    }

    @Override
    public Flowable<Company> getCompany(String stockSymbol) {
        return Flowable.create(emitter -> {
            Company company = new Company();
            String stockData = mSharedPreferences.getString(KEY_COMPANY_PREFIX + stockSymbol, null);

            if (!TextUtils.isEmpty(stockData)) {
                company = mGson.fromJson(stockData, Company.class);
            }
            emitter.onNext(company);
            emitter.onComplete();

        }, BackpressureStrategy.LATEST);
    }

    @Override
    public Flowable<List<Statistic>> getStatistic(boolean forceLoad, String range, String symbol) {
        return Flowable.create(emitter -> {
            List<Statistic> statistics = new ArrayList<>();
            String statisticJsonString = mSharedPreferences.getString(KEY_STATISTIC_PREFIX + symbol, null);
            Type type = new TypeToken<List<Statistic>>() {
            }.getType();
            if (!TextUtils.isEmpty(statisticJsonString)) {
                statistics = mGson.fromJson(statisticJsonString, type);
            }
            emitter.onNext(statistics);
            emitter.onComplete();
        }, BackpressureStrategy.LATEST);
    }

    @Override
    public void addCompany(Company company) {
        mSharedPreferences.edit().putString(KEY_COMPANY_PREFIX + company.getSymbol(), mGson.toJson(company)).apply();
    }

    @Override
    public void addStatistic(String stockSymbol, List<Statistic> statistic) {
        mSharedPreferences.edit().putString(KEY_STATISTIC_PREFIX + stockSymbol, mGson.toJson(statistic)).apply();
    }

    @Override
    public void updateStockSymbol(String stockSymbol) {
        mSharedPreferences.edit().putString(KEY_SELECTED_STOCK_SYMBOL, stockSymbol).apply();
    }

    @Override
    public void clearData() {
        mSharedPreferences.edit().clear().apply();
    }
}