package com.shubh.stocker.response;

import com.shubh.stocker.beans.Company;
import com.shubh.stocker.beans.Statistic;
import com.shubh.stocker.beans.Stock;

import java.util.List;

public class StockResponse {
    private Stock stock;
    private Company company;
    private List<Statistic> statistics;

    public StockResponse(Stock stock, Company company, List<Statistic> statistics) {
        this.stock = stock;
        this.company = company;
        this.statistics = statistics;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Statistic> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<Statistic> statistics) {
        this.statistics = statistics;
    }
}
