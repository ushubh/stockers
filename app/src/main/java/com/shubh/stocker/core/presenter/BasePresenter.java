package com.shubh.stocker.core.presenter;

import com.shubh.stocker.core.view.BaseView;

public interface BasePresenter<V extends BaseView> {

    void onViewAttached(V view);

    void onViewDetached();

    V getView();
}
