package com.shubh.stocker.core.view;

public interface BaseContentView extends BaseView {

    void showProgress();

    void hideProgress();

    void showRetry();

    void hideRetry();

    void hideContent();

    void showContent();

    void showEmptyScreen();

    void showError();
}
