package com.shubh.stocker.core.fragments;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shubh.stocker.R;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;


public abstract class ContentFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    protected View mRootView;

    @BindView(R.id.progress_bar)
    @Nullable
    ProgressBar mProgressBar;

    @BindView(R.id.message)
    @Nullable
    TextView mMessageTextView;

    @BindView(R.id.retry_button)
    @Nullable
    Button mRetryButton;

    @BindView(R.id.contentPanel)
    @Nullable
    View mContentPanel;

    @LayoutRes
    protected abstract int getContentLayoutResId();

    protected boolean enablePullToRefresh() {
        return false;
    }

    protected void loadData() {
    }

    @Override
    public void onRefresh() {
    }

    protected void onRetry() {

    }

    @Override
    protected final View getFragmentView(LayoutInflater inflater, ViewGroup container) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_content, container, false);
        View contentView = inflater.inflate(getContentLayoutResId(), container, false);

        if (contentView == null) {
            return viewGroup;
        }

        if (enablePullToRefresh()) {
            setRefreshLayout(contentView, viewGroup);
        } else {
            viewGroup.addView(contentView);
        }
        mRootView = viewGroup;
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    private void setRefreshLayout(View contentView, ViewGroup viewGroup) {
        SwipeRefreshLayout swipeRefreshLayout = new SwipeRefreshLayout(getContext());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(viewGroup.getLayoutParams());
        swipeRefreshLayout.setLayoutParams(layoutParams);
        viewGroup.addView(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.addView(contentView);
    }


    public void setMessage(@StringRes int id) {
        if (mMessageTextView != null) {
            mMessageTextView.setVisibility(View.VISIBLE);
            mMessageTextView.setText(id);
        }
    }

    @Optional
    @OnClick(R.id.retry_button)
    protected void onRetryClicked() {
        onRetry();
    }

    public void showProgress() {
        getActivity().runOnUiThread(() -> {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    public void hideProgress() {
        getActivity().runOnUiThread(() -> {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    public void showRetry() {
        if (mRetryButton != null) {
            mRetryButton.setVisibility(View.VISIBLE);
        }
    }

    public void hideRetry() {
        if (mRetryButton != null) {
            mRetryButton.setVisibility(View.GONE);
        }
    }

    public void showError() {
        setMessage(R.string.request_failed);
    }

    public void hideContent() {
        getActivity().runOnUiThread(() -> {
            if (mContentPanel != null) {
                mContentPanel.setVisibility(View.GONE);
            }
        });
    }

    public void showContent() {
        getActivity().runOnUiThread(() -> {
            if (mContentPanel != null) {
                mContentPanel.setVisibility(View.VISIBLE);
            }
        });
    }

    public void showEmptyScreen() {
        if (mMessageTextView != null) {
            mMessageTextView.setText(R.string.no_item_exists);
            mMessageTextView.setVisibility(View.VISIBLE);
        }
    }
}
