package com.shubh.stocker.core.presenter;


import com.shubh.stocker.core.view.BaseView;

import java.lang.ref.WeakReference;

import timber.log.Timber;

public abstract class AbsPresenter<V extends BaseView> {

    private WeakReference<V> mViewReference;

    public AbsPresenter(V view) {
        Timber.tag(getClass().getSimpleName());
        mViewReference = new WeakReference<>(view);
    }

    public void onViewAttached(V view) {
        mViewReference = new WeakReference<>(view);
    }

    public void onViewDetached() {
        if (mViewReference != null) {
            mViewReference.clear();
            mViewReference = null;
        }
    }

    public V getView() {
        if (mViewReference == null) {
            throw new IllegalStateException("Presenter is not attached to view. Call onViewAttached from activity/fragment ");
        }
        return mViewReference.get();
    }
}
