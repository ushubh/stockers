package com.shubh.stocker.scheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;

import com.shubh.stocker.utils.CollectionUtils;

import java.util.List;

public class SchedulerUtil {

    private static final int REFRESH_INTERVAL_IN_MILLIS = 60 * 60 * 1000;

    private static final int FLEX_TIME_IN_MILLIS = 5 * 10 * 1000;

    public static boolean isJobScheduled(Context context, int jobId) {

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler == null) {
            return false;
        }

        List<JobInfo> allPendingJobs = jobScheduler.getAllPendingJobs();
        if (CollectionUtils.isEmpty(allPendingJobs)) {
            return false;
        }

        for (JobInfo jobInfo : allPendingJobs) {
            if (jobInfo.getId() == jobId) {
                return true;
            }
        }
        return false;
    }


    public static int schedule(Context context, int jobId, Class serviceClass) {
        ComponentName mServiceComponent = new ComponentName(context, serviceClass);
        JobInfo.Builder builder = new JobInfo.Builder(jobId, mServiceComponent);

        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setRequiresDeviceIdle(true);
        builder.setRequiresCharging(false);
        builder.setPersisted(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setPeriodic(REFRESH_INTERVAL_IN_MILLIS, FLEX_TIME_IN_MILLIS);
        } else {
            builder.setPeriodic(REFRESH_INTERVAL_IN_MILLIS);
        }

        // Schedule job
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null)
            return jobScheduler.schedule(builder.build());

        return -1;
    }
}
