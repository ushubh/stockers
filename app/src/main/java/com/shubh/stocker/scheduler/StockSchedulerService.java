package com.shubh.stocker.scheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;

import com.shubh.stocker.data.StockRepository;
import com.shubh.stocker.response.StockResponse;

import java.util.Calendar;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class StockSchedulerService extends JobService {

    @Inject
    StockRepository mStockRepository;

    private Disposable disposable;

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidInjection.inject(this);
        Timber.tag(this.getClass().getName());
        Timber.d("Service created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public boolean onStartJob(final JobParameters params) {

        int hourOfTheDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if (9 > hourOfTheDay || hourOfTheDay > 17) {
            return true;
        }

        if (mStockRepository == null) {
            return true;
        }

        disposable = mStockRepository.getSelectedStockSymbol()
                .flatMap(stockSymbol ->
                        Flowable.zip(
                                mStockRepository.getCompany(stockSymbol).subscribeOn(Schedulers.io()),
                                mStockRepository.loadStock(true, stockSymbol).subscribeOn(Schedulers.io()),
                                mStockRepository.getStatistic(false, "1d", stockSymbol).subscribeOn(Schedulers.io()),
                                (company, stock, statistics) -> new StockResponse(stock, company, statistics)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stockResponse -> jobFinished(params, false), Timber::e, () -> Timber.d("Load stock complete"));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (disposable != null) {
            disposable.dispose();
        }
        return false;
    }
}