package com.shubh.stocker.settings;

import com.shubh.stocker.core.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingFragmentModule {

    @Provides
    @PerFragment
    SettingsContract.View provideSettingView(SettingsFragment settingsFragment) {
        return settingsFragment;
    }
}
