package com.shubh.stocker.settings;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.shubh.stocker.R;
import com.shubh.stocker.core.fragments.ContentFragment;

import javax.inject.Inject;

import butterknife.BindView;

public class SettingsFragment extends ContentFragment implements SettingsContract.View {

    @Inject
    SettingsContract.Presenter mSettingPresenter;

    @BindView(R.id.stockSymbolEditText)
    EditText mStockSymbolEditText;

    @Override
    protected int getContentLayoutResId() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSettingPresenter.onViewDetached();
    }

    @Override
    protected void loadData() {
        mSettingPresenter.getStockSymbol();
    }

    @Override
    public void onDataReceived(String stockSymbol) {
        mStockSymbolEditText.setText(stockSymbol);
    }

    @Override
    public void onSymbolSave() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_settings_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.save) {
            mSettingPresenter.saveStockSymbol(mStockSymbolEditText.getText().toString());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
