package com.shubh.stocker.settings;

import android.os.Bundle;

import com.shubh.stocker.R;
import com.shubh.stocker.core.activity.BaseActivity;


public class SettingsActivity extends BaseActivity {
    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_settings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
