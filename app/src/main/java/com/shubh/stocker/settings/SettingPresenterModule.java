package com.shubh.stocker.settings;

import com.shubh.stocker.core.scope.PerFragment;

import dagger.Binds;
import dagger.Module;


@Module(includes = SettingFragmentModule.class)
public abstract class SettingPresenterModule {

    @Binds
    @PerFragment
    public abstract SettingsContract.Presenter settingdPresenter(SettingPresenter settingPresenter);
}
