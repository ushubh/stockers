package com.shubh.stocker.settings;

import com.shubh.stocker.core.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SettingsActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {SettingPresenterModule.class})
    abstract public SettingsFragment settingsFragment();
}
