package com.shubh.stocker.settings;

import com.shubh.stocker.core.presenter.BasePresenter;
import com.shubh.stocker.core.view.BaseContentView;


public interface SettingsContract {

    interface View extends BaseContentView {
        void onDataReceived(String stockSymbol);

        void onSymbolSave();
    }

    interface Presenter extends BasePresenter<View> {
        void getStockSymbol();

        void saveStockSymbol(String stockSymbol);
    }
}
