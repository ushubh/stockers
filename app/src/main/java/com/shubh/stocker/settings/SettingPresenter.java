package com.shubh.stocker.settings;

import com.shubh.stocker.core.presenter.AbsPresenter;
import com.shubh.stocker.core.scope.PerFragment;
import com.shubh.stocker.data.StockRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@PerFragment
public class SettingPresenter extends AbsPresenter<SettingsContract.View> implements SettingsContract.Presenter {

    private StockRepository mStockRepository;

    private Disposable mDisposable;

    @Inject
    SettingPresenter(StockRepository stockRepository, SettingsContract.View view) {
        super(view);
        this.mStockRepository = stockRepository;
    }

    @Override
    public void getStockSymbol() {
        mDisposable = mStockRepository.getSelectedStockSymbol()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stockSymbol -> getView().onDataReceived(stockSymbol),
                        throwable -> getView().showError(),
                        () -> {
                        });
    }

    @Override
    public void onViewDetached() {
        super.onViewDetached();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    @Override
    public void saveStockSymbol(String stockSymbol) {
        mStockRepository.fetchCompanyDetails(stockSymbol)
                .doOnSubscribe(subscription -> getView().showProgress())
                .doOnTerminate(() -> getView().hideProgress())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(company -> getView().onSymbolSave(),
                        throwable -> getView().showError(),
                        () -> getView().hideProgress());
    }
}
